{
  "22900": {
    "desc": "Chemical Engineering Thermodynamics I"
  },
  "31000": {
    "desc": "Introduction to Materials Science"
  },
  "31101": {
    "desc": "Analysis of Chemical Engineering Processes"
  },
  "34200": {
    "desc": "Transport Phenomena II"
  },
  "34500": {
    "desc": "Unit Operations I"
  },
  "34600": {
    "desc": "Unit Operations II"
  },
  "49600": {
    "desc": "Chemical Engineering Design Project"
  },
  "49803": {
    "desc": "Honors Research in Chemical Engineering I"
  },
  "49808": {
    "desc": "Nanomaterials"
  },
  "49903": {
    "desc": "Honors Research in Chemical Engineering II"
  }
}