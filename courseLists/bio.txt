{
  "10004": {
    "desc": "Human Biology"
  },
  "10100": {
    "desc": "Biological Foundations I"
  },
  "10200": {
    "desc": "Biological Foundations II"
  },
  "20600": {
    "desc": "Introduction to Genetics"
  },
  "20700": {
    "desc": "Organismic Biology"
  },
  "22800": {
    "desc": "Ecology and Evolution"
  },
  "22900": {
    "desc": "Cell and Molecular Biology"
  },
  "24700": {
    "desc": "Hum Anat/Physiol 1"
  },
  "24800": {
    "desc": "Hum Anat/Physiol 2"
  },
  "A4810": {
    "desc": "Introduction to Epigenetics"
  },
  "A6000": {
    "desc": "Animal Behavior"
  },
  "A8300": {
    "desc": "Laboratory In Biotechnology"
  },
  "B4540": {
    "desc": "Sensory Perception"
  },
  "B9901": {
    "desc": "Thesis Research"
  },
  "B9902": {
    "desc": "Thesis Research"
  },
  "V1401": {
    "desc": "Cell Biology"
  },
  "V2302": {
    "desc": "Neuroscience II"
  },
  "V5003": {
    "desc": "Develepomntl Biology"
  },
  "V9100": {
    "desc": "Colloquium"
  },
  "V9202": {
    "desc": "Advanced Study"
  },
  "V9203": {
    "desc": "Advanced Study"
  },
  "V9204": {
    "desc": "Advanced Study"
  },
  "V9308": {
    "desc": "Neuropharmacology"
  }
}