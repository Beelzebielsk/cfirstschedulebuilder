- Schedule:
	- one of: su|mo|tu|we|th|fr|sa
		- Each one of these holds an array of session objects.
		- Schedules here are hardcoded as weekly. Maybe that's a bad idea.
		- Each session is a thing that happens on that day.
- Session:
	- start: start time
	- end: end time. Not a weektime, just a normal time, because classes
		don't cross days (yet).
	- professor: The person who teaches.
	- room: A location to meet.
- Section:
	- course name
	- id: CUNYFirst gives them one
	- course description
	- discipline
	- number
	- Schedule
- Course:
	- A grouping of sections that represent one class. You won't sign up
		for courses, you'll sign up for sections. However, you have to
		sign up for sections that belong to a certain set of sections to
		advance in college (ie. you've gotta sign up for sections of your
		core courses to graduate).
	- Courses will come up during schedule building, when we have to
		decide which sections you need to take, and which sections are
		*possible* to take.
	- Name
	- Discipline
	- Course Number
	- Credits
	- Prerequisites
	- Corequisites

Basically, I'm thinking of doing things by taking unions of schedules.
Each section comes packed with a mini-schedule and I build the full
schedule by taking unions of schedules until I combine all the classes
that I wanted to combine. Should make creating a schedule pretty easy.

I can keep the same schedule conflict algorithm from before:

- create an array of section id's where the array keys are weektimes
	(a number formed of a numeric day of week followed by a numeric
	time) which represent starting and ending times of everything.
	Then, go through the array checking to see if there's ever a pair of
	entries that aren't the same.
