# About

This is a set of utilities for using the CUNYFirst website without
having to put up with it.

## Currently Available Modules

- fetch: Scrape the CUNYFirst website for information using the
	*Selenium* browser automator. At this moment, it can navigate to
	CUNYFirst's class search page, perform a search on a given college,
	term, and subject, and return the results as raw text.
- parse: Parse information from web scrapings. Designed with the intent
	of being easily extensible, so that parsing output from different
	scrapes is as easy as possible.

## Planned Modules

This project is meant to be run on a web server, to scrape for
information regularly, and to create reports/alerts for that
information.

- Running as a daemon: Create a scheduler that will run in the
	background and run various tasks at user-defined intervals. Is
	intended to be easily configured, and to reload the configuration
	on-the-fly, so that changing which tasks are run, and when they are
	run is as easy as modifying a configuration file.
- Alerts Service: Create ways of creating notifications when tasks run
	by the daemon meet certain criteria, such as a class having free
	seats, or a certain section of a class having free seats, or even when
	details of class sections are not "TBA" (meaning that more information
	has come out about the class. The intent is for the user to write out
	the test, and that the method for creating these tests should be both
	simple and expressive. It could be an expression in a JSON query
	language, and the test could just be checking to see if the JSON
	matches the query. The details are still being worked out. These
	notifications could be emails sent to an email address, or through
	some other service.
