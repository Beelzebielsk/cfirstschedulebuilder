# Next Steps

## Jobs Module

- Set up a jobs module. Something that will perform jobs automatically.
	- It should have a job to perform, which ouputs something. The
		something could have side effects.
	- A test should then be run using the output of the job, and if the
		test comes back true, then a mail is sent.
		- Since there are tests that could be run against the same job,
			create a sequence of tests instead. All the tests will get run
			against the test. Each test should have the opportunity to alter
			their outgoing email (if needed).
	- The job shoud specify the job function, the test function, and the
		basic mail fields. The test should be able to alter the mail fields
		as it needs to. The stored mail fields are just a 'template' for the
		mail.
		- Should also specify a list of recipients per test. Initially, I
			ought to just be able to write functionality into the application.
			Eventually, people ought to just be able to register for alerts.
	- Figure out how to have a daemon running, or how to arrange for the
		tests to happen using something like systemd. You can tune the
		frequency of checks using systemd, and just have the systemd script
		run the whole thing

Sending mail using your gmail in python.

~~~ 
import smtplib

to = 'mkyong2002@yahoo.com'
gmail_user = 'mkyong2002@gmail.com'
gmail_pwd = 'yourpassword'
smtpserver = smtplib.SMTP("smtp.gmail.com",587)
smtpserver.ehlo()
smtpserver.starttls()
smtpserver.ehlo
smtpserver.login(gmail_user, gmail_pwd)
header = 'To:{}\nFrom:{}\nSubject:testing \n'.format(to, gmail_user)
print header
msg = header + '\n this is test msg from mkyong.com \n\n'
smtpserver.sendmail(gmail_user, to, msg)
print 'done!'
smtpserver.close()
~~~

## Making Parsers like Visitors

If I can collect the parsing methods in a class, or a class-like object,
then I can colelct parsing behaviors, and use inheritance to choose
which behaviors take precedence over which. The behaviors should largely
split up in similar ways to how they do in visitors. It will be a bit
more finely-grained, since some details of the document structure are
more complicated than what it takes to parse them.

## Cleaning up the Visitors

Move to parsing based on named rules when possible. That seems much more
robust in the face of changes, just in case details have to change
later.

## History Storage

It's imperative to capture the history of the searches, because CCNY
sucks at keeping classes around. I'd like records of old searches when
things change around suddenly.

Based on the simple research below, my capability to store history drops
dramatically as I try to do more things. I wouldn't want to store more
than 1 GB at a time, and I'd prefer to leave it at about 10 - 100 MB.
I will have to throw away a lot of history if I make more searches, or
search for more things.

The research below also captures something else crucial: whether or not
I keep the result of these searches around for a long time, if I write
them to disk at all, that's how many writes will happen per year. That
will dramatically kick up the rate at which my storage would decay. I
don't have a real server, so this is not an option. The best I could do
is host this on a server that could actually handle all of this, like a
google cloud container or something.

### Size of Searches

I'm going to do some Fermi approximation here. Everything will be in
powers of 10.

Quantity                            | Actual    | Rounded
Unparsed Search (Single Discipline) | 20 KB     | 1E4 B
Parsed Search (Single Discipline)   | 50 KB     | 1E5 B
Num Disciplines (All)               | 102       | 1E2
Num Important Disciplines (Some)    |           | 1E1
Num Searches (Mild)                 | 1-3       | 1E0
Num Searches (Busy)                 | 5-10      | 1E1
Num Searches (Distributed)          | 1E3 - 1E4 | 1E4
Num Days Mild                       | ~120      | 1E2
Num Days Busy                       | ~120      | 1E2

Search Type * Disc * \# Searches * \# Days = Result
	Parsed    * All  * Distributed * 1E2     = If everyone had access
	(1E5 B)   * 1E2  * 1E4         * 1E2     = 1E11 B (100 GB)
 Unparsed   * All  * Distributed * 1E2     = If everyone had access
	(1E4 B)   * 1E2  * 1E4         * 1E2     = 1E10 B (10 GB)
	Parsed    * All  * Mild + Busy * 1E2     = Worst Case
	(1E5 B)   * 1E2  * (1E0 + 1E1) * 1E2     = 1E9 B (1 GB)
 Unparsed   * All  * Mild + Busy * 1E2     = Slightly Better
  (1E4 B)   * 1E2  * (1E0 + 1E1) * 1E2     = 1E8 B (100 MB)
 Unparsed   * Some * Mild + Busy * 1E2     = Conservative
  (1E4 B)   * 1E1  * (1E0 + 1E1) * 1E2     = 1E7 B (10 MB)

- Size of search for one discipline: 100KB (rounded from ~50KB).
- There are 105 different disciplines in CCNY, which is about 1E2. That
	puts the size of a full search of all classes at about 1E7 B.
- I probably wouldn't search evenly throughout the year. I've got no
	reason to do searches before it's time to plan out classes, so the
	first two months of each semester won't see any activity. Vacations
	might, if I'm waiting on a class. However, I'm sure people check for
	classes less often during vacations, so I probably wouldn't check
	incessantly. Perhaps once or twice a day.
	- So, the times where I'm definitely not busy is 4 months out the
		year. Nothing will happen during that time. This is about 120 days.
	- For the remaining two months of each semester, I expect to be busy.
		That's another 120 days, just about.
	- For each vacation, which is another four months total, I expect to
		be Mildly Busy.
