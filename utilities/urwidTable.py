#!/usr/bin/env python
import urwid

class TableColumn(urwid.ListBox):
    _sizing = frozenset(['flow'])
    def rows(self, size, focus=False):
        height = 10
        print(height)
        #print("Look here!", size, "Stop Looking!", sep='\n')
        #for widget in self.body.contents:
            #height = height + widget.rows(size, focus=focus)
        return 100

stuff = ['a', 'b', 'ccc']
texts = list(map( lambda i: urwid.Text(i), stuff))

#fill = urwid.Filler( urwid.Text("Hello World!") )
fill = urwid.Filler( urwid.Text('\n'.join(stuff )) )
#fill = urwid.Filler( urwid.ListBox( texts ) )
#fill = urwid.Filler( urwid.ListBox( urwid.SimpleFocusListWalker(texts) ) )
#fill = urwid.Filler( TableColumn( texts ) )

print( urwid.Filler._selectable )
print( urwid.Filler._sizing )
print( urwid.ListBox._selectable )
print( urwid.ListBox._sizing )
#print( urwid.SimpleFocusListWalker._selectable )
#print( urwid.SimpleFocusListWalker._sizing )

loop = urwid.MainLoop(fill)
loop.run()
