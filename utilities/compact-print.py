import json
import sys

inFile = 'output.json'
with open(inFile) as f: data = json.load(f)

courses = sys.argv[1:]
def printCourse(course):
    print("{}: {}".format(course, data[course]['name']))
    printSections(course)
def printSections(course):
    for section in data[course]['sections']:
        print("\t{}:".format(section['id']))
        printSessions(section)
def printSessions(section):
    for session in section['sessions']:
        print("\t\t{times}, {professor}".format(**session))

for course in courses: printCourse(course)
