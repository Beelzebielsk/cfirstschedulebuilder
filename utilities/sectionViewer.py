#!/usr/bin/env python

# View sections for specified classes in a GUI.

import json
import urwid
import getopt
import sys
import argparse

############################################################
## Arguments : {{{
############################################################

# TODO: Handle debugging.
parser = argparse.ArgumentParser()
parser.add_argument("-i", "--input", type=argparse.FileType('r'),
                    required=True, 
                    metavar="JSON-formatted list of courses")
parser.add_argument("-d", "--debug", action="store_true", 
                    help="Not yet implemented.")

args = parser.parse_args()
with args.input as f:
    courseList = json.load(f)

############################################################
## }}}
############################################################

class ClassList(urwid.WidgetWrap):
    def __init__(self, widgets, debug=False):
        widgetList = [*widgets, urwid.Edit("", "")]
        self.debug = debug
        if debug:
            self.keydisplay = urwid.Text("")
            widgetList.append(self.keydisplay)
        self.list = urwid.ListBox(widgetList)
        urwid.WidgetWrap.__init__(self, self.list)

    def setDBGText(self, text):
        if self.debug:
            self.keydisplay.set_text(text)
    signals = ['finishList']

    def getCourseList(self):
        return [x.edit_text for x in self.list.body.contents if \
                isinstance(x, urwid.Edit) ]

    def keypress(self, size, key):
        self.setDBGText(key)
        if key == 'enter':
            self.list.body.contents.append(urwid.Edit("", ""))
        elif key == 'meta enter':
            self._emit('finishList', self.getCourseList())
            self.setDBGText('finishList')
        elif key == 'meta backspace':
            nix = self.list.focus_position;
            self.setDBGText(str(nix))
            wList = self.list.body.contents
            if len(wList) <= 2:
                return
            self.list.body.contents = wList[0:nix] + wList[nix+1:]
        #elif key == 'esc':
            #raise urwid.ExitMainLoop()
        else:
            return super().keypress(size, key)

class CoursePane(urwid.WidgetWrap):
    def __init__(self, courseList):
        # JSON Course List:
        self.courseList = courseList

        header = urwid.Text("Sections")
        self.mainList = urwid.ListBox([])
        body = urwid.Filler( self.mainList, height=('relative', 100) )
        self.frame = urwid.Frame(body, header=header)
        urwid.WidgetWrap.__init__(self, self.frame)

    def setCourses(self, courseNames):
        courses = \
            [prettyCourse(self.courseList[x]) for x in courseNames\
            if x in self.courseList ]
        self.mainList.body.contents = courses

    # Done to prevent the listener function from receiving callee as
    # first argument, while still keeping listener function in class.
    def updateListener(self):
        return lambda _, emmitedCourses: self.setCourses(emmitedCourses)

def makeClassList():
    body = [urwid.Text("Class List"), urwid.Divider('-')]
    return ClassList(body)

############################################################
## Prettifying Functions : {{{
############################################################

def prettySession(session):
    times = urwid.Text(session['times'])
    rooms = urwid.Text(session['room'])
    professors = urwid.Text(', '.join(session['professor']))
    return urwid.Columns([times, rooms, professors])

# Section Name
# Sessions
def prettySection(section):
    sectId = urwid.Text( section['id'] )
    sessions = [ prettySession(x) for x in section['sessions'] ]
    status = urwid.Text( section['status'] )
    sessionPile = urwid.Pile(sessions)
    return urwid.Columns([ (10, sectId), sessionPile, (10, status) ])

def titleFill(title, fillChar='-'):
    text = urwid.Text(title)
    columns = [
        urwid.Divider(fillChar),
        ('pack', text),
        urwid.Divider(fillChar),
    ]
    return urwid.Columns(columns)

def prettyCourse(course):
    #header = urwid.Text( course[0]['courseId'] )
    #print(course)
    courseId = course['sections'][0]['courseId']
    #desc = '' if courseId not in descriptions\
            #else descriptions[courseId]['desc']
    desc = course['name']
    header = titleFill('{} : {}'.format(courseId, desc))
    body = [prettySection(x) for x in course['sections'] ]
    return urwid.Pile([header, *body])

def VDivider(fillChar='|'):
    return (1, urwid.SolidFill(fillChar))

############################################################
## }}}
############################################################

def key_handler(key):
    if key=='esc':
        raise urwid.ExitMainLoop();

courses = CoursePane(courseList)
classList = makeClassList()
urwid.connect_signal(classList, 'finishList', courses.updateListener())

columns = [(15, classList), VDivider(), courses]
fill = urwid.Padding( urwid.Columns(columns) )
loop = urwid.MainLoop(fill, unhandled_input=key_handler)
loop.run()


# STRETCH:
# - Add a way to select sections.
# - Add a way to export information about each section (such as the id,
#   so that registration is easier).

# Session Info: Text
# Session : Columnns of Session Info, as tall as tallest.
# Section : ListBox of Sessions
# Section Pane: ListBox of Sections
