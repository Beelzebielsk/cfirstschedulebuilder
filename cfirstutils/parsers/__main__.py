import argparse
import sys
import json
from . import parse
parser = argparse.ArgumentParser(description=
            """Take input and parse it. The input is typically
            something from the cunyfirst.fetch module, but it
            doesn't absolutely have to be. The options specify how
            to parse the input.""")
parser.add_argument("-o", "--output", default=sys.stdout, 
        type=argparse.FileType('w'), help=
        """The destination of the output of the program. If not
        provided, then printed to standard output.""")
parser.add_argument("-i" ,"--input", default=sys.stdin, 
        type=argparse.FileType('r'), help=
        """Source of input for the program. If not provided, then
        taken from standard input.""")
actions = parser.add_mutually_exclusive_group(required=True)
actions.add_argument("--courseList", action="store_true", help=
        """Produce a list of courses from a class search.""")
actions.add_argument("--sections", action="store_true", help=
        """Produce a JSON formatted dictionary of courses, each with
        a list of the available sections mentioned in the input."""
        )

args = parser.parse_args()
with args.input as f:
    text = f.read()
if args.sections:
    visitor = ['phantom', 'compact', 'search']
elif args.courseList:
    visitor = ['phantom', 'courseList', 'search']

with args.output as f:
    output = parse(text, inputType='phantomSearch', outputType=visitor)
    json.dump(output, f, sort_keys=True, indent=2)
