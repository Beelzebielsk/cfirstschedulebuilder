from arpeggio import *
from arpeggio import RegExMatch as _

# This file is a collection of base parse rules. They could show up
# anywhere, in any context, and they should all be parsed the same.
# They're the primitive pieces of any text-based info that ought to make
# up something about classes.

####################  Rules for Section Pieces
# These are all the pieces that make up a section, before they're put
# together in the more specific modules for parsers.

def CourseNumber(): return _(r'(?i)[A-Z0-9]{4,5}')
def SectionType():  return _(r'(?i)regular|winter|summer')
def SectionName():  return Optional("*"), alphaNum, "-", alphaNum
def Discipline():   return _(discpilineExp)
def status():       return _(r'(?i)open|closed')

####################  Rules for Rooms
# Rooms generally consist of a building name, then a formatted number (a
# number with some building-specific prefix). The format for NAC is an
# exception. It just has a building abbreviation and a room number.

def rooms(): return OneOrMore(room)
def room(): return [shepardRoom, steinmanRoom, nacRoom,
                    marshakRoom, spitzerRoom, broadwayRoom,
                    baskervilleRoom]

def baskervilleRoom(): return "Baskervill", roomNumber
def broadwayRoom():    return "25 Bway", roomNumber, "-", roomNumber
def spitzerRoom():     return "SSA", roomNumber
def shepardRoom():     return "Shepard", Optional("S-"), roomNumber
def steinmanRoom():    return "Steinman", Optional("ST"), roomNumber
def nacRoom():         return "NAC", number, '/', roomNumber
def marshakRoom():     return "Marshak", Optional("MR"), roomNumber
def roomNumber():      return alphaNum

#################### Rules for Names
def Instructors():     return Instructor, ZeroOrMore(",", Instructor)
def Instructor():      return Sequence(nl, OneOrMore(name), ws=nonl)
# Some names have apostrophes and/or hyphens in them.
def name():            return (alphaWord,
                               ZeroOrMore(_(r"['-]"), alphaWord))

#################### Rules for Times.
def dateRanges():      return OneOrMore(dateRange)
def dateRange():       return date, "-", date
def date():            return number, "/", number, "/", number
def timeRanges():      return OneOrMore(weekTimeRange)
def weekTimeRange():   return days, hour, "-", hour
def hour():            return number, ":", number, ["AM", "PM"]
def days():            return _(r'(?i)(mo|tu|we|th|fr|sa|su)+')
def day():             return _(r'(?i)mo|tu|we|th|fr|sa|su')

#################### Rules for words.
def nonSeparators():   return OneOrMore(nonSeparator)
def nonSeparator():    return _(r'([^- \t\n\r]|((?<=\w)-(?=\w)))+')
def phrase():          return OneOrMore(alphaNum)
def alphaWords():      return OneOrMore(alphaWord)
def alphaNum():        return _(r'\w+')
def alphaWord():       return _(r'[a-zA-Z]+')
def number():          return _(r'\d+')
def nl():              return '\n'

# Collection of whitespace which is not a newline.
nonl = '\t\r '
disciplines = [
    'aes',
    'amst',
    'anth',
    'anthe',
    'arab',
    'arch',
    'art',
    'arte',
    'asia',
    'astr',
    'beng',
    'bio',
    'bioe',
    'blst',
    'bme',
    'ce',
    'cert',
    'che',
    'chem',
    'cheme',
    'chin',
    'cl',
    'clas',
    'clss',
    'csc',
    'cu ba',
    'cuny',
    'cwe',
    'eas',
    'ease',
    'eco',
    'ecoe',
    'edce',
    'edls',
    'edsc',
    'edse',
    'educ',
    'ee',
    'elec',
    'engl',
    'engle',
    'engr',
    'esl',
    'fiqws',
    'fquan',
    'fren',
    'germ',
    'grk',
    'heb',
    'hist',
    'histe',
    'hndi',
    'hum',
    'ias',
    'intl',
    'ir',
    'ital',
    'jap',
    'jwst',
    'laar',
    'lals',
    'lang',
    'lat',
    'mam',
    'math',
    'mathe',
    'mca',
    'me',
    'med',
    'meds',
    'mhc',
    'mis',
    'msci',
    'mus',
    'ncuny',
    'nss',
    'pa',
    'perm',
    'phil',
    'phys',
    'physe',
    'port',
    'psc',
    'psm',
    'psy',
    'sci',
    'scie',
    'soc',
    'soce',
    'span',
    'spane',
    'spch',
    'sped',
    'ssc',
    'stabd',
    'sus',
    'thtr',
    'ud',
    'ul',
    'usso',
    'wciv',
    'whum',
    'wiu',
    'ws',
    'yid',
]
discpilineExp = r'\b(?i)({})\b'.format('|'.join(disciplines))
