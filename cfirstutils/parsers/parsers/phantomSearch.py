from arpeggio import *
from arpeggio import RegExMatch as _
from .base import *
from .baseSearch import *

# This is a collection of the specialized parse rules that are just
# for parsing text obtained from a dump of a phantomJS page when
# browsing a class search.

# The top rule is pretty much always going to be the same. Parse the
# courses.
def top(): return OneOrMore(OtherCrap, Course), Optional(OtherCrap), EOF
def Course(): return (
        ManyCourseHeadings, OneOrMore(Section,
                                      Optional(OtherCrap)))
# Literally, the form of unimportant, skippable text. There is a lot
# of text between the important pieces which is useless for me.
def OtherCrap(): return ZeroOrMore(
        Not([SectionHeading, CourseHeading]), _(r'\S*'))
def ManyCourseHeadings(): return OneOrMore(CourseHeading)
# Apparently, class names can be very long and include hyphens unto
# themselves. Though, to this point, full class names have yet to go
# beyond a single line in the fetched output.
def CourseHeading(): return Sequence(ZeroOrMore(nl), Discipline,
                                     CourseNumber, "-", 
                                     OneOrMore(
                                         nonSeparators,
                                         Optional("-")),
                                     ws=nonl)
