from arpeggio import *
from arpeggio import RegExMatch as _
from .base import *

# This is a collection of the common rules that are shared between the
# parsers for web searches of classes.

# The "1" is there as part of a new format to parse. Not sure how that
# happened.
def Section(): return (SectionHeading, StrangeOne, number,
                       SectionName, SectionType, Sessions, status)
# I'm not sure where these numbers started coming from, nor do I know
# their purpose. This rule is here entirely to be thrown away by
# visitors.
def StrangeOne(): return Optional("1")
def Sessions(): return OneOrMore(Not(status), SessionItem)
def SectionHeading(): return (Optional("Row"), "Class", "Section",
        "Days", "&", "Times", "Room", "Instructor", "Meeting",
        "Dates", "Status")
def SessionItem(): 
    return ["TBA", weekTimeRange, room, Instructors, dateRange]
