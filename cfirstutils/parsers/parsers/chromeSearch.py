from arpeggio import *
from arpeggio import RegExMatch as _
from .base import *
from .baseSearch import *

# This is a collection of the specialized parse rules that are just for
# parsing text obtained from copying and pasting the whole webpage from
# google chrome when doing a web search for classes.

# The top rule is pretty much always going to be the same. Parse the
# courses.
def top(): return OneOrMore(OtherCrap, Course), Optional(OtherCrap), EOF
def Course(): return (CourseHeading,
        OneOrMore(Section, Optional(OtherCrap)))
# Literally, the form of unimportant, skippable text. There is a lot of
# text between the important pieces which is useless for me.
def OtherCrap(): return ZeroOrMore(
        Not([("Collapse", "section"), ("Class", "Section")]),
        _(r'\S*'))
def CourseHeading(): return Sequence(ZeroOrMore(nl),
        ("Collapse", "section"),
        Discipline, CourseNumber,
        "-", nonSeparators, "-", nonSeparators,
        nl, ws=nonl)
