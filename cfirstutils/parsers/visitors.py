#!/usr/bin/env python
import arpeggio

import re
from itertools import zip_longest

############################################################
## Common Visitation Functions: {{{
############################################################

def joiner(joinString=' ', prefix='', suffix=''):
    return lambda self, node, children:\
            prefix + joinString.join(children) + suffix
# If you put 'None' as a slice index, it's like you don't use it.
# So, the default is a copy of the whole array.
# 's' and 'e' are start and end respectively
def defaultVisit(s=None, e=None):
    return lambda self,node,children: children[s:e] if s or e else children

def ignore():
    return lambda self,node,children: None

############################################################
## }}}
############################################################

# While there is a strict separation of some rules in the parsing
# module, I won't mimic the same strict separation here. The form of
# some of the semantic objects may change from source to source, but
# their treatment doesn't. I will make separations here based on whether
# a different way of analyzing things semantically is needed.

# Contains visitation rules for everything in `parsers.parsers.base`.
# No matter what, this is going to be a base for every other visitor.
class BaseVisitor(arpeggio.PTNodeVisitor):
    visit_nl = ignore()
    visit_phrase = joiner()
    visit_StrangeOne = ignore()
    def visit_nonSeparator(self, node, children):
        if self.debug:
            print(node)
            print(children)
            print(dir(node))
        return str(node)
    visit_nonSeparators = joiner(' ')

    # Returns time as number.
    def visit_hour(self, node, children):
        time = int(children[0] + children[1])
        if children[0] != '12':
            time += 1200 if children[-1].upper() == "PM" else 0
        return time

    # Return an object with days as keys, and each key
    # holds a dictionary with important session characteristics.
    # For now, just put session start and end times.
    def visit_weekTimeRange(self, node, children):
        if children[0] == "TBA":
            return children[0]
        day = r'(?i)mo|tu|we|th|fr|sa|su'
        days = re.findall(day, children[0])
        time = {'start' : children[1], 'end' : children[2] }
        # Place the time dict in each 'day' key.
        return dict(zip_longest(days, [], fillvalue=time))
    visit_timeRanges = defaultVisit()
    visit_date = joiner('/')
    def visit_dateRange(self, node, children):
        # Will be exactly two children, start and end date.
        return dict( zip(['start', 'end'], children) )
    visit_dateRanges = defaultVisit();
    visit_name = joiner('-')
    visit_names = joiner()
    visit_SectionName = joiner('-')

    visit_shepardRoom     = joiner(prefix="Shepard ")
    visit_steinmanRoom    = joiner(prefix="Steinman ")
    visit_marshakRoom     = joiner(prefix="Marshak ")
    visit_spitzerRoom     = joiner(prefix="SSA ")
    visit_baskervilleRoom = joiner(prefix="Baskerville ")
    visit_nacRoom         = joiner(prefix="NAC ", joinString='/')
    visit_broadwayRoom    = joiner(prefix="Broadway ", joinString='-')
    def visit_baskervilleRoom(self, node, children):
        return "Baskerville " + ''.join(children)
    visit_rooms = defaultVisit()
    visit_rooms = defaultVisit()
    visit_Instructor = joiner()
    visit_Instructors = defaultVisit()

# The base for searches. Covers everything in
# `parsers.parsers.baseSearch`, and every rule in all the search parsers
# that are visited in the same way (meaning that processing them
# semantically is the same process).
class SearchBaseVisitor(arpeggio.PTNodeVisitor):
    def visit_Sessions(self, node, children):
        sessionFields = 4
        totalSessions = len(children) / sessionFields
        if int(totalSessions) != totalSessions:
            raise SyntaxError("Expected session information"
                              "in multiples of {}! Received {}"
                              .format(sessionFields, len(children)))
        totalSessions = int(totalSessions)
        times = children[0:None:totalSessions]
        sessions = []
        keys = ['room', 'professor', 'meetingDates']
        for i in range(0,totalSessions):
            fields = children[i:None:totalSessions]
            times = fields[0];
            info = fields[1:]
            sessionInfo = dict(zip(keys, info))
            sessionInfo.update([['times', times]])
            sessions.append(sessionInfo)
        return sessions

    # Makes sure to ignore everything from it. There is no important
    # information in this heading, save for the heading signifying
    # where things start/stop.
    visit_SectionHeading = ignore()
    def visit_Section(self, node, children):
        return {
            'id' : str(children[0]),
            'name' : children[1],
            'type' : children[2],
            'sessions' : children[3],
            'status' : children[4]
        }

    def visit_Course(self, node, children):
        courseinfo = children[0] # CourseHeading
        courseId = "{discipline}{courseNum}".format(**courseinfo)

        for section in children[1:]:
            try:
                section.update([('courseId', courseId)])
            except AttributeError:
                print("Attribute Error Occurred")
                print("Problem Section:", section)
                for s in children[1:]:
                    print(s)
                raise AttributeError
        return children[1:]
    visit_top = defaultVisit()
    visit_OtherCrap = ignore()


class ChromeVisitor(arpeggio.PTNodeVisitor):
    def visit_CourseHeading(self, node, children):
        return { 
            'discipline' : children[0].lower(),
            'courseNum' : children[1],
            'name' : children[3],
        }

class PhantomVisitor(arpeggio.PTNodeVisitor):
    def visit_CourseHeading(self, node, children):
        return { 
            'discipline' : children[0].lower(),
            'courseNum' : children[1],
            'name' : children[2],
        }
    # In general, it seems that multiple consecutive course headings
    # are all headings for the exact same course.
    def visit_ManyCourseHeadings(self, node, children):
        return children[0]

class CompactVisitor(arpeggio.PTNodeVisitor):
    def visit_weekTimeRange(self, node, children):
        if children[0] == "TBA":
            return children[0]
        return "{} {} - {}".format(*children[0:3])
    def visit_Course(self, node, children):
        courseinfo = children[0]
        normalValue = super().visit_Course(node, children)
        return {
            'name' : courseinfo['name'],
            'sections' : normalValue
        }
    def visit_top(self, node, children):
        courses = {}
        for course in children:
            courseName = course['sections'][0]['courseId']
            if courseName not in courses:
                courses.update([(courseName, course)])
            else:
                courses[courseName]['sections'].extend(
                        course['sections'])
        return courses

class CourseList(arpeggio.PTNodeVisitor):
    def visit_Course(self, node, children):
        courseinfo = children[0]
        courseId = "{discipline}{courseNum}".format(**courseinfo)
        return { 
                    courseId : { 
                        'desc' : courseinfo['name'] 
                    }
               }
    def visit_top(self, node, children):
        courses = {}
        for course in children:
            courses.update(course)
        return courses

visitations = {
    'base'       : BaseVisitor,
    'search'     : SearchBaseVisitor,
    'chrome'     : ChromeVisitor,
    'phantom'    : PhantomVisitor,
    'compact'    : CompactVisitor,
    'courseList' : CourseList,
}
# The way that this should work is you specify which behavior you want
# with a list of behaviors. Earlier behaviors take precedence over later
# behaviors, and the last behavior to be used is the `BaseVisitor`.
# So, for instance, if I were to specify ['compact', 'search',
# 'phantom'] , then I should get a visitor that will output compact
# results, and will be able to process stuff from a text dump of a
# browser search, and will process stuff from a PhantomJS text dump
# specifically. If I specify just ['search', 'phantom'], then it will
# still work, but the results won't be as compact anymore. The behavior
# will be completely determined by Python's multiple inheritance model.

############################################################
## Main : {{{
############################################################

if __name__ == "__main__":
    from arpeggio import visit_parse_tree as process_tree
    #print("I'm in main!")
    import sys
    args = sys.argv
    #print(sys.argv)

    grammarFile = 'search.peg' if len(args) < 2 else args[1]
    parseFile = '../classLists/cscFa2017.txt' if len(args) < 3 else args[2]
    actual = open(parseFile,'r').read()

    grammar = open(grammarFile, 'r').read();
    exampleParser = ParserPEG(grammar, 'Course')

    examples = [
        #"csc 1000",
        #"Collapse section CSC 10200 - Introduction to Computing CSC 10200 - Introduction to Computing ",
        #" MoWe 11:00AM ",
        #" MoWe 11:00PM ",
        #"Douglas Troeger",
        #"Chi-Him Liu",

    """
    Collapse section CSC 10200 - Introduction to Computing CSC 10200 - Introduction to Computing 
        Class   Section Days     & Times    Room    Instructor  Meeting Dates   Status   
        37699
        CC1-LEC
        Regular
        MoWe 11:00AM - 11:50AM
        Mo 2:00PM - 3:40PM
        TBA
        TBA
        Douglas Troeger
        Chi-Him Liu
        01/30/2017 - 05/26/2017
        01/30/2017 - 05/26/2017
        Open

        """
    ]


    try:
        #trees = list( map( lambda i: exampleParser.parse(i), examples) )
        trees = [ exampleParser.parse(x) for x in examples ]
    except arpeggio.NoMatch as e:
        print("Error occurred in example!")
        print(e)
        #print( dir(e) )

    try:
        exampleTrees = [ exampleParser.parse(x) for x in examples ]
        actualTree = parser.parse(actual)
        for tree in exampleTrees:
            print( process_tree(tree, SearchVisitor(debug=True)) )
        print( process_tree(actualTree, SearchVisitor(debug=True)) )
    except arpeggio.NoMatch as e:
        print("Error occurred in actual!")
        print(e)
        #print( dir(e) )

############################################################
## }}}
############################################################
