from .parsers import (
        chromeSearch, phantomSearch
        )
from .visitors import visitations
import arpeggio

class Parsers:
    chromeSearch = arpeggio.ParserPython(chromeSearch)
    phantomSearch = arpeggio.ParserPython(phantomSearch)

    @staticmethod
    def getChromeSearchDebug():
        return arpeggio.ParserPython(chromeSearch, debug=True)
    @staticmethod
    def getPhantomSearchDebug():
        return arpeggio.ParserPython(phantomSearch, debug=True)

def createVisitor(visitList):
    """
    Creates a visitor out of list of valid keys of the visitation
    dictionary. The order of the visitors is the precedence of the
    behaviors of the visitors. If one visitor comes before another
    (earlier in the sequence), then all the visitation methods of the
    earlier one.

    Parameters
    ----------
    visitList : sequence
        A sequence of strings that represent visitors to combine in one
        class.

    Returns
    -------
    visitorClass : visitor class instance
        It's an instance of a class whose subclasses are the classes
        specififed in visitList (as strings), in the order that they
        were specified.
    """

    visitorClasses = [visitations[c] for c in visitList]
    if visitorClasses[-1] != visitations['base']:
        visitorClasses.append(visitations['base'])
    class __(*visitorClasses): pass
    return __()

def parse(input, inputType='phantomSearch', outputType=['base']):
    if inputType == 'chromeSearch':
        parser = Parsers.chromeSearch
    elif inputType == 'phantomSearch':
        parser = Parsers.phantomSearch

    visitor = createVisitor(outputType)
    return arpeggio.visit_parse_tree(parser.parse(input), visitor)
