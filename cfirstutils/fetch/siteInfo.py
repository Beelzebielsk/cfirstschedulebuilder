info = {
    'search' : {
        'url' :
            'https://home.cunyfirst.cuny.edu/psp/cnyepprd/GUEST/HRMS/c/COMMUNITY_ACCESS.CLASS_SEARCH.GBL?FolderPath=PORTAL_ROOT_OBJECT.HC_CLASS_SEARCH_GBL&IsFolder=false&IgnoreParamTempl=FolderPath%25252cIsFolder',
        'institution' :
            'select#CLASS_SRCH_WRK2_INSTITUTION$31$.PSDROPDOWNLIST',
        'term' : 
            'select#CLASS_SRCH_WRK2_STRM$35$.PSDROPDOWNLIST',
        'subject' :
            'select#SSR_CLSRCH_WRK_SUBJECT_SRCH$0.PSDROPDOWNLIST',
        'status' :
            'input#SSR_CLSRCH_WRK_SSR_OPEN_ONLY$5.PSCHECKBOX',
        'statusLabel' :
            'label#SSR_CLSRCH_WRK_SSR_OPEN_ONLY_LBL$5.PSCHECKBOX',
        'minimum-units' :
            'input#SSR_CLSRCH_WRK_UNITS_MINIMUM$12.PSEDITBOX',
        'maximum-units' :
            'input#SSR_CLSRCH_WRK_UNITS_MAXIMUM$12.PSEDITBOX',
        'search-button' :
            'a#CLASS_SRCH_WRK2_SSR_PB_CLASS_SRCH.SSSBUTTON_CONFIRMLINK',
        'frameSelector' : 'frame[name=TargetContent]',
        'frameName' : 'TargetContent',
        'resultsPage' : 'body div[page="SSR_CLSRCH_RSLT"]',
        'searchPage' : 'body div[page="SSR_CLSRCH_ENTRY"]',
        'waitAnimation' : '#WAIT_win0',
    },
    'classInfo' : {
        'frameSelector' : 'frame[name=TargetContent]',
        'requisites' : 'span#SSR_CLS_DTL_WRK_SSR_REQUISITE_LONG',
        # Refers to the span that contains the text.
        'description' : 'span#DERIVED_CLSRCH_DESCRLONG',
        'searchResultsButton' : 'a#CLASS_SRCH_WRK2_SSR_PB_BACK',
        'capacityInfo' : 'table#ACE_SSR_CLS_DTL_WRK_GROUP3',
        'waitAnimation' : '#WAIT_win0',
    },
    'catalog' : {
        'url' : 'https://home.cunyfirst.cuny.edu/psp/cnyepprd/GUEST/HRMS/c/COMMUNITY_ACCESS.SSS_BROWSE_CATLG.GBL?FolderPath=PORTAL_ROOT_OBJECT.HC_SSS_BROWSE_CATLG_GBL4&%2520IsFolder=false&IgnoreParamTempl=FolderPath%25252cIsFolder',
        'institution' : '#DERIVED_SSS_BCC_INSTITUTION$114$',
        'institutionChangeButton' : '#DERIVED_SSS_BCC_SSS_PB_CHANGE',
        'waitAnimation' : '#WAIT_win0',
        'startingLetters' : {
            'a' : '#DERIVED_SSS_BCC_SSR_ALPHANUM_A',
            'b' : '#DERIVED_SSS_BCC_SSR_ALPHANUM_B',
            'c' : '#DERIVED_SSS_BCC_SSR_ALPHANUM_C',
            'd' : '#DERIVED_SSS_BCC_SSR_ALPHANUM_D',
            'e' : '#DERIVED_SSS_BCC_SSR_ALPHANUM_E',
            'f' : '#DERIVED_SSS_BCC_SSR_ALPHANUM_F',
            'g' : '#DERIVED_SSS_BCC_SSR_ALPHANUM_G',
            'h' : '#DERIVED_SSS_BCC_SSR_ALPHANUM_H',
            'i' : '#DERIVED_SSS_BCC_SSR_ALPHANUM_I',
            'j' : '#DERIVED_SSS_BCC_SSR_ALPHANUM_J',
            'k' : '#DERIVED_SSS_BCC_SSR_ALPHANUM_K',
            'l' : '#DERIVED_SSS_BCC_SSR_ALPHANUM_L',
            'm' : '#DERIVED_SSS_BCC_SSR_ALPHANUM_M',
            'n' : '#DERIVED_SSS_BCC_SSR_ALPHANUM_N',
            'o' : '#DERIVED_SSS_BCC_SSR_ALPHANUM_O',
            'p' : '#DERIVED_SSS_BCC_SSR_ALPHANUM_P',
            'q' : '#DERIVED_SSS_BCC_SSR_ALPHANUM_Q',
            'r' : '#DERIVED_SSS_BCC_SSR_ALPHANUM_R',
            's' : '#DERIVED_SSS_BCC_SSR_ALPHANUM_S',
            't' : '#DERIVED_SSS_BCC_SSR_ALPHANUM_T',
            'u' : '#DERIVED_SSS_BCC_SSR_ALPHANUM_U',
            'v' : '#DERIVED_SSS_BCC_SSR_ALPHANUM_V',
            'w' : '#DERIVED_SSS_BCC_SSR_ALPHANUM_W',
            'x' : '#DERIVED_SSS_BCC_SSR_ALPHANUM_X',
            'y' : '#DERIVED_SSS_BCC_SSR_ALPHANUM_Y',
            'z' : '#DERIVED_SSS_BCC_SSR_ALPHANUM_Z',
            '0' : '#DERIVED_SSS_BCC_SSR_ALPHANUM_0',
            '1' : '#DERIVED_SSS_BCC_SSR_ALPHANUM_1',
            '2' : '#DERIVED_SSS_BCC_SSR_ALPHANUM_2',
            '3' : '#DERIVED_SSS_BCC_SSR_ALPHANUM_3',
            '4' : '#DERIVED_SSS_BCC_SSR_ALPHANUM_4',
            '5' : '#DERIVED_SSS_BCC_SSR_ALPHANUM_5',
            '6' : '#DERIVED_SSS_BCC_SSR_ALPHANUM_6',
            '7' : '#DERIVED_SSS_BCC_SSR_ALPHANUM_7',
            '8' : '#DERIVED_SSS_BCC_SSR_ALPHANUM_8',
            '9' : '#DERIVED_SSS_BCC_SSR_ALPHANUM_9',
        },
    }
}

# TODO: Consider moving this to something like an inheritance model. If
# a key is not present in a dictionary, it looks through containing
# dictionaries. This way, if there's information that's common to
# several pages, then I can store it all in once place.

# Selecting classes from the catalogue:
# 1. Find the first letter of the discipline, and select the letter
#    selector which matches that letter.
# 2. Each collapse/uncollapse is a link (<a>). You could click on them,
#    and you could filter by link text.
# 3. Uncollapsing one creates a table nearby (and under).
#    - The id of the uncollapsed table and the id of the link element
#      share something in common. They both end in the same number. I'd
#      rather not depend on that.
# 4. Waiting for change is covered by waiting for the loading indicator
#    to appear and disappear.
# 5. I can then look for the class code, if I know the class code ahead
#    of time. If I don't, then I'm not sure how I'd find the table in a
#    very general way. I don't like depending too much on selectors.
#    Those are basically an implementation detail, and I'm sure that
#    they're subject to change at any time.

# Selecting a specific class in a search:
# 1. I can search for a link with the section number as text, and look
#    for the first table element which contains that element. That's an
#    xpath selcctor. From there, I can just fetch all the text out of
#    it. That first table should contain all of the relevant section
#    information. It might not contain other crap, though, so I might
#    have the change the parsers a bit to make the first OtherCrap rule
#    optional.
