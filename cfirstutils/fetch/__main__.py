from .fetch import (
        fetchSearchChrome, fetchSearchPhantom,
        )
import argparse
import sys

parser = argparse.ArgumentParser(description=
            ("Remotely fetch information from CunyFirst's "
                "website. Can currently get class search "
                "information, either from the Chrome Browser "
                "or using PhantomJS."))
parser.add_argument("-o", "--output", default=sys.stdout, 
        type=argparse.FileType('w'),
        help= ("Output file name. If ommitted, then "
                "will print to standard output."))
actions = parser.add_mutually_exclusive_group(required=True)
actions.add_argument("--search", nargs=4, 
        metavar=("method", "institution", "term",
                    "subject/discipline"),
        help=
            ("Fetch a class search remotely. Specify the "
                "method for search, and the search parameters. "
                "The available search methods are:\n"
                "-  phantom : Use PhantomJS\n"
                "-  chrome : Use Chromium or Google Chrome.\n"
                "The search parameters are, in order, the college, "
                "the semester, and the discipline."))
args = parser.parse_args()

method, college, term, disc = args.search
output = args.output
if method not in ['chrome', 'phantom']:
    raise SyntaxError("Search Methods must be one of 'chrome' "
                        "or 'phantom'")
searchFunction = (fetchSearchChrome if method == 'chrome'
                    else fetchSearchPhantom)
with args.output as f:
    print(searchFunction(college, term, disc), file=f)
