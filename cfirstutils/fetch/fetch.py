import re
import string
# For manipulating clipboard.
import pyperclip
from selenium import webdriver
from selenium.common.exceptions import (
        TimeoutException,
        NoSuchElementException
        )
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.keys import Keys

from .siteInfo import info as siteInfo

# For escaping css selectors. Dollar signs aren't valid (apparently?) in
# css selectors.
def escapeSelectors(selector):
   return re.sub(r'(\$)', r'\\\1', selector)

# 'select' elements (drop-down lists) have their options specified as
# children with the <option> tag. I can search for elements by their
# inner text using XPath.
def getOptionWithText(select, text):
    """
    Pass in a selenium element for a select box, and text that one of
    the options in the select should contain. Returns a selenium element
    that represents the first option which contains that text.

    Parameters
    ----------
    select : selenium WebElement
        The WebElement which represents a select on the page.
    text : string
        Text which is in one of the options of the select element.
        Case insensitive (only for the english alphabet characters).
        May be any valid substring of one of the options in the
        select. 
    Returns
    -------
    option : selenium WebElement
        The first option element found which is a child of the select
        element and has the given text as a valid substring. If no
        such element is found, then return TODO: What gets returned
        here?
    """
    # - '.' is the text content of the current node, it seems.
    # - 'translate(., {}, {})' turns the text content to all upper case
    #   (only for the ascii letters a-z).
    # - We compare the given text to the lowercased version of the text
    #   content.
    try:
        return select.find_element_by_xpath(
                'option[contains('
                'translate(., "{}", "{}"),'
                '"{}")]'.format(
                    string.ascii_lowercase,
                    string.ascii_uppercase,
                    text.upper()))
    except NoSuchElementException as e:
        return None

def setOptionWithText(select, text, alwaysClick=False):
    """
    setOptionWithText(select, text, alwaysClick=False):

    Clicks on the given option.

    Parameters
    ----------
    select : selenium WebElement
        A select element.
    text : string
        A string contained within an option of the select element.
    alwaysClick : boolean, optiona
        If True, then will click on the option whether or not the
        option is already selected. 

    Returns
    -------
    wasClicked : boolean
        Returns true if the option was clicked, returns False
        otherwise.
    """
    option = getOptionWithText(select, text)
    if option is None:
        available = select.find_elements_by_xpath('option');
        message = (
            "No option with text '{}' found inside of '{}'. "
            "Available options were: {}").format(
                        text, select.text,
                        [s.text for s in available])
        raise NoSuchElementException(msg=message)
    if alwaysClick or not option.is_selected():
        option.click()
        return True
    return False

# Create a dict whose fields are the names of form elements, and whose
# values are the elements themsleves. Just for convenience, makes
# accessing the elements easier.
def createFormDict(driver, selectors, selectorsDict):
    """
    Given a dictionary of selectors for elements, and a list of keys
    into this dictionary, return a dictionary of webelements for the
    given selectors.

    Parameters
    ----------
    driver : selenium webdriver
        The driver instance from which to extract the elements.
    selectors : sequence
        A sequence of keys into the selectors dictionary.
    selectorsDict : dict
        A dictionary of (element name, selector) pairs. The names in the
        selectors argument must correspond to the keys of this
        dictionary.

    Returns
    -------
    elementDict : dict
        A dictionary of (element name, Web Element) pairs. The values of
        the dictionaries are the Selenium Web Elements that correspond
        to the selectors from selectorsDict with the same dictionary
        key.
    """
    return dict([(e, driver.find_element_by_css_selector(
                    escapeSelectors(selectorsDict[e])))
                 for e in selectors])

def waitForAnimation(driver):
    """
    Actions finish rendering when the animation button appears, then
    disappears. This waits until it appears, then disappears. If it
    never appears, then it does not try to check if it disappeared.

    Parameters
    ----------
    driver : selenium webdriver
        Current webdriver instance.

    Returns
    -------
    None
    """
    try:
        WebDriverWait(driver, 1, .1).until(
                EC.visibility_of_element_located(
                    ('css selector', siteInfo['search']['waitAnimation'])))
        WebDriverWait(driver, 10, .1).until(
                EC.invisibility_of_element_located(
                    ('css selector', siteInfo['search']['waitAnimation'])))
    except TimeoutException:
        pass

def getAllTextInPage(element):
    element.send_keys(Keys.CONTROL, 'a')
    element.send_keys(Keys.CONTROL, 'c')
    return pyperclip.paste()

# Specific to Chrome Browser.
# Function to fetch a class search with the given parameters.
def fetchSearchChrome(institution, term, subject):

    formElements = [
        'institution',
        'term',
        'subject',
        'status',
        'minimum-units',
        'maximum-units',
        'search-button',
    ]


    driver = webdriver.Chrome()
    driver.get(siteInfo['search']['url'])

    # Wait until the page loads...
    # driver object, timeout, poll frequency.
    WebDriverWait(driver, 10, 1).until(
        EC.frame_to_be_available_and_switch_to_it(
            siteInfo['search']['frameName']))

    form = createFormDict(driver, formElements, siteInfo['search'])

    # TODO: Catch error for unavailable institution.
    setOptionWithText(form['institution'], institution)
    waitForAnimation(driver)
    form = createFormDict(driver, formElements, siteInfo['search'])
    clicked = setOptionWithText(form['term'], term)
    if clicked:
        waitForAnimation(driver)
    # TODO: Catch error for unavailable subject.
    setOptionWithText(form['subject'], subject)
    # TODO: Change this to a set to off, so that you don't have to
    # program according to initial state.
    # 4. Toggle status off.
    form['status'].click()
    form['minimum-units'].send_keys('0')
    form['maximum-units'].send_keys('100')
    form['search-button'].click()

    # 8. Wait for Search results
    waitForAnimation(driver)

    body = driver.find_element_by_css_selector('body')
    pageText = getAllTextInPage(body)
    driver.quit()
    return pageText

# Specific to PhantomJS headless browser.
# Function to fetch a class search with the given parameters.
def fetchSearchPhantom(institution, term, subject):
    """
    Gets search results using PhantomJS.

    Parameters
    ----------
    institution : string
        The institution whose classes you'd like to look at.
    term : string
        The term in which the classes will be held.
    subject : string
        The subject (discipline) of the classes. For example, the
        discpline of "CSC 10300" is "csc" or "computer science".

    Returns
    -------
    searchText : string
        The raw text of the webpage for a search that satisfies the
        given criteria.
    """

    formElements = [
        'institution',
        'term',
        'subject',
        'status',
        'minimum-units',
        'maximum-units',
        'search-button',
    ]

    driver = webdriver.PhantomJS()
    # Won't render results without a call like this. The specific
    # parameters are arbitrary, but I chose something fairly large for
    # useful screenshots.
    driver.set_window_size(800, 400)
    driver.get(siteInfo['search']['url'])
    WebDriverWait(driver, 10, 1).until(
        EC.frame_to_be_available_and_switch_to_it(
            siteInfo['search']['frameName']))

    form = createFormDict(driver, formElements, siteInfo['search'])
    setOptionWithText(form['institution'], institution)
    waitForAnimation(driver)
    form = createFormDict(driver, formElements, siteInfo['search'])
    clicked = setOptionWithText(form['term'], term)
    if clicked:
        waitForAnimation(driver)
        form = createFormDict(driver, formElements, siteInfo['search'])
    # TODO: Catch error for unavailable subject.
    setOptionWithText(form['subject'], subject)
    form['status'].click()
    form['minimum-units'].send_keys('0')
    form['maximum-units'].send_keys('100')
    form['search-button'].click()
    waitForAnimation(driver)

    body = driver.find_element_by_css_selector('body')
    # Replace images by the alt text, so that the text is picked up by
    # body.text.
    driver.execute_script("""
        var images = document.querySelectorAll('img[alt="Open"], img[alt="Closed"], img[alt="Wait List"]');
        for (var i = 0; i < images.length; i++) {
            var image = images[i];
            var altText = image.attributes['alt'].value;
            var newElement = document.createElement('p');
            var newTextNode = document.createTextNode(altText);
            newElement.appendChild(newTextNode);
            var parent = image.parentNode;
            parent.replaceChild(newElement, image);
        }
        """)
    pageText = body.text
    driver.quit()
    return pageText

# Hiding website idiosyncracies:
# - I can create a wrapper class for selenium webelements which contains
#   the basic semantics for how to do certain things, like waiting.
#   Would also know to refetch the element should it go stale, so it
#   should know the selector and context in which the element was
#   originally found.
# - I can create another class that basically works like the "formStuff"
#   dictionary, allowing me to quickly reference each piece of a part of
#   the website.
# - With a combination of these two, I should no longer need to specify
#   waits, and when they happen manually. I just include the high-level
#   steps of navigating the website.

# TODO: Clean this up.
# - See if you can hide the idiosyncrasies of the website away.
#   - Some events on the page seem to cause elements you've looked for
#     before to become invalid. See if you can automatically handle
#     that. Perhaps create a class that catches the stale error and
#     regenerates the element when the stale error happens.
#   - Some events on the page seem to cause other elements to change
#     entirely. For instance, without an institution and term, there are
#     no options in nearly all of the other selects on the page.
#   - All navigation happens through substitution what appears on the
#     page. The page itself doesn't refresh or change. The URL doesn't
#     change. Just the elements on the page. So, when you switch a frame
#     once, you seem to stay on that frame entirely.
#   - In general, each sort of action may or may not require a wait. If
#     necessary, waits can be put at the end of everything.
#     - Clicks of selects require waits.
#     - Filling input boxes don't.
#     - Clicking checkboxes doesn't.
#     - Clicking site buttons does.
#     - Any element can potentially go stale.

# TODO: Extend this.
# - Fetch the information for a given class, like prereqs, credits, and
#   when it is commonly offered, and what designation it is. Maybe even
#   current number of seats filled, and seats left.
#   - Whatever you can't fetch, create an easy way to create/update it.
#     It wouldn't be impossible to have this information be
#     crowd-sourced by the users. If a user notices an error, they
#     correct it once for everyone.
# - Make a data structure that represents a site map. Basically, it
#   would be a graph, and every node would have information on a page.
#   - Selectors for elements of note.
#   - Methods for transitioning to neighboring nodes. This would be a
#     dictionary where the keys are neighboring nodes, and the values
#     are functions which receive the webdriver and navigate that driver
#     to the neighboring node.
#   - Using this, you could fetch a large number of things all at once,
#     trying to use the fewest connections possible. 
# - Alternatively, you could also store the information that can be
#   grabbed at each given page with each node. This way, you create
#   paths through the website for a given set of data, then you use the
#   page information at each node to fetch the data.
#   - If you can get threading working, a minimum spanning tree would be
#     a better approach. Create a thread for each possible path.

# TODO: Consider how you'd distribute such capabilities.
# - If you distribute the capability to remotely interact with CunyFirst
#   to everyone, then there's going to be a lot of redundant searches.
#   That's pointless. You don't want to crash the thing... or do you?
#   That might be a nice lesson for sticking us with this bullshit.
#   - It might be better to have a handful of personally-controlled
#     nodes that fetch info from cunyfirst, and then others can just ask
#     for the fetched info from those nodes.
#   - The information can be stamped with a timestamp, noting how old it
#     is. If users want more current information, then they update the
#     information for everyone. However, most people would probably
#     abuse the shit out of this and push for having the most up-to-date
#     information anyhow.
#   - At the same time, it wouldn't be impossible to update something
#     like this to be placed at college. I get a server going and it
#     just serves everyone. We just let CunyFirst be a back-end (worst
#     possible back-end ever) to a much more friendly front-end that
#     allows one to look for classes. When they finally want to
#     register, they just punch the section numbers into the shopping
#     cart and go.

# TODO: Consider multithreading this, since page loads take a while and
# such.
# - Figure out how to resolve the IP only once. There's no point in
#   resolving it multiple times per visit.
# - Figure out how best to do the multithreading, and how to combine the
#   results. Put them all in different files at first? Should I parse
#   each one separately, then combine the parsed results, or should I
#   combine then parse once? My money is on the second. JSON parsers
#   will be written well, and I'm sure parsing the highly strucured text
#   will be easier than parsing one huge chunk of poorly structured
#   text.

# All the form fields seem to go stale when the page has to change.
