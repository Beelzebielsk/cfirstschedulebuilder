import argparse
import json
import sys
from .fetch import fetchSearchPhantom
from .parsers import parse
from arpeggio import NoMatch

parser = argparse.ArgumentParser(description=
        "Fetch information from cunyfirst's website and parse it."
        "Writes the unparsed output and parsed output to two files.")
parser.add_argument('-d', '--discipline', type=str, nargs='+',
                    required=True)
parser.add_argument('--institution', type=str, required=True)
parser.add_argument('-s', '--semester', type=str, required=True)

args = parser.parse_args()

fetchInfo = fetchSearchPhantom
def parseInfo(fetchedInput):
    try:
        return parse(fetchedInput, inputType='phantomSearch',
                    outputType=['phantom', 'compact', 'search']);
    except NoMatch as e:
        with open('fail.txt', 'w') as f:
            print(fetchedInput, file=f)
        raise e


institution = args.institution
semester = args.semester
disciplines = args.discipline
ouputs = []
finalOutput = {}
for disc in disciplines:
    output = parseInfo(fetchInfo(institution, semester, disc))
    finalOutput.update(output)
json.dump(finalOutput, sys.stdout, indent=2)
